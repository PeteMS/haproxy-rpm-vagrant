#!/bin/bash

sudo yum -y install rpmdevtools && rpmdev-setuptree;
sudo yum -y install pcre-devel gcc make openssl-devel;
cp /vagrant/haproxy.spec /root/rpmbuild/SPECS/haproxy.spec;
wget http://haproxy.1wt.eu/download/1.5/src/haproxy-1.5.1.tar.gz -O /root/rpmbuild/SOURCES/haproxy-1.5.1.tar.gz;
rpmbuild -bb /root/rpmbuild/SPECS/haproxy.spec --define "release 1";
cp /root/rpmbuild/RPMS/x86_64/haproxy-1.5.1-1.x86_64.rpm /vagrant/;