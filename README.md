rpm-haproxy-vagrant
===========

A Vagrant setup to create a RPM for Haproxy.

Useful if you're not on a Red Hat system (such as OSX in my case) and need an easy way of creating RPMs for use on Red Hat servers.

```
vagrant up rpmmaker
```

The RPM should then be created in this directory.